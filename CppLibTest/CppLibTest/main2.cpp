//  main.cpp
//  CppLibTest
//
//  Created by cat on 9/19/18.

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "AronLib.h"
// #include "/Users/cat/myfile/bitbucket/cpplib/AronPrint.h"


// UPDATE: Sun 28 Nov 13:05:16 2021  
// KEY: cpplib test, test AronLib.h
// NOTE: Run on xcode

// UPDATE: Sunday, 23 April 2023 13:47 PDT
// Speed up the compilation
// main2.cpp contains all the new cpp function in $b/cpplib/AronLib.h
// NOTE: Use new haskell-cpp-compile instead of cpp_compile.sh 
// haskell-cpp-compile main2.cpp

TEST_CASE( "main2", "[AronLib2]") {
    using namespace MatrixVector;
    using namespace Utility;
    using namespace AronLambda;
    using namespace Algorithm;
    using namespace AronGeometry;
    using namespace SpaceTest;
	{
	  {
		vector<vector<float>> v2 = {{1, 2},
									{3, 4}
		};
		float n = det2<float>(v2);
		float expn = -2;
		assert(n == expn);
	  }
	  {
		{
		  pp("crossProduct");
		  vector<float> v1 = {1, 2, 3};
		  vector<float> v2 = {1, 2, 3};
		  vector<float> v3 = crossProduct<float>(v1, v2);
		  pp(v3);
		}
		{
		  pp("crossProduct");
		  vector<float> v1 = {1, 2, 3};
		  vector<float> v2 = {2, 3, 4};
		  vector<float> v3 = crossProduct<float>(v1, v2);
		  pp(v3);
		}
	  
	  }
	  {
		pp("det2");
		vector<vector<float>> v1 = {{}};
		vector<vector<float>> v2 = {{1, 2},
									{3, 4}
		};
		float n = det2<float>(v2);
		printf("n=%f\n", n);
		pp(len(v2));
		pp(len(v2[0]));
		pp(len(v1[0]));
	  }
	  {
		fw("dotProduct");
		vector<float> v1 = {1, 2, 3};
		vector<float> v2 = {1, 2, 3};
		float n = dotProduct<float>(v1, v2);
		printf("dotProduct=%f\n", n);
	  }
	}
	{
	  {
		string s = "a";
		string pad = "-";
		int maxLen = 0;
		string ret = padLeft(s, pad, maxLen);
		string expectedS = "a";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = "-";
		int maxLen = 1;
		string ret = padLeft(s, pad, maxLen);
		string expectedS = "a";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = "-";
		int maxLen = 4;
		string ret = padLeft(s, pad, maxLen);
		string expectedS = "---a";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = " ";
		int maxLen = 1;
		string ret = padLeft(s, pad, maxLen);
		string expectedS = "a";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = " ";
		int maxLen = 4;
		string ret = padLeft(s, pad, maxLen);
		string expectedS = "   a";
		assert(ret == expectedS);
	  }
	}
	{
	  {
		string s = "a";
		string pad = "-";
		int maxLen = 0;
		string ret = padRight(s, pad, maxLen);
		string expectedS = "a";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = "-";
		int maxLen = 1;
		string ret = padRight(s, pad, maxLen);
		string expectedS = "a";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = "-";
		int maxLen = 4;
		string ret = padRight(s, pad, maxLen);
		string expectedS = "a---";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = " ";
		int maxLen = 1;
		string ret = padRight(s, pad, maxLen);
		string expectedS = "a";
		assert(ret == expectedS);
	  }
	  {
		string s = "a";
		string pad = " ";
		int maxLen = 4;
		string ret = padRight(s, pad, maxLen);
		string expectedS = "a   ";
		assert(ret == expectedS);
	  }
	}

	{
	  {
		fw("getCol 1");
		vector<vector<float>> v = {
		  {1, 2, 3},
		  {4, 5, 6}
		};
		vector<float> vt = getCol(0, v);
		vector<float> expVt = {1, 4};
		assert(vt == expVt);
	  }
	  {
		fw("getCol 2");
		vector<vector<float>> v = {
		  {1, 2, 3},
		  {4, 5, 6}
		};
		vector<float> vt = getCol(2, v);
		vector<float> expVt = {3, 6};
		assert(vt == expVt);
	  }
	  {
		fw("getRow 1");
		vector<vector<float>> v = {
		  {1, 2, 3},
		  {4, 5, 6}
		};
		vector<float> vt = getRow(1, v);
		vector<float> expVt = {4, 5, 6};
		assert(vt == expVt);
	  }
								  
	}
	
}
