//  main.cpp
//  CppLibTest
//
//  Created by cat on 9/19/18.

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "AronLib.h"
// #include "/Users/cat/myfile/bitbucket/cpplib/AronPrint.h"


// UPDATE: Sun 28 Nov 13:05:16 2021  
// KEY: cpplib test, test AronLib.h
// NOTE: Run on xcode

class Polygon{
protected:
    int m;
    int n;
public:
    Polygon(){
        m = 0;
        n = 0;
    }
    Polygon(int m, int n){
        this -> m = m;
        this -> n = n;
    }
};

class Rectangle : public Polygon{
public:
    Rectangle(){}
    Rectangle(int m, int n){
        Polygon(m, n);
    }
public:
    double area(){
        return m*n;
    }
};

class Triangle : public Polygon{
public:
    Triangle(){}
    Triangle(int m, int n){
        Polygon(m, n);
    }
public:
    double area(){
        return m*n/2;
    }
};

unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}

TEST_CASE( "AronLib.h", "[AronLib1]") {
    using namespace MatrixVector;
    using namespace Utility;
    using namespace AronLambda;
    using namespace Algorithm;
    using namespace AronGeometry;
    using namespace SpaceTest;
    if (true){
    {
        vector<int> v{1, 2, 3};
        std::vector<int> vec = geneVector(1, 3);
        assert( vec == v );

        vector<int> v1 = {0};
        std::vector<int> vec1 = geneVector(0, 0);
        assert( vec1 == v1 );

        vector<int> v2 = {1};
        std::vector<int> vec2 = geneVector(1, 1);
        assert( vec2 == v2 );

    }
    
    {
        std::vector<int> vec = geneVector(1, 10);
        std::vector<int> v = AronLambda::map([](auto x){return x + 1;}, vec);
        assert( v == geneVector(2, 11) );

        std::vector<int> vec1 = geneVector(0, 0);
        std::vector<int> v1 = AronLambda::map([](auto x){return x + 1;}, vec1);
        assert( v1 == geneVector(1, 1) );

    }

    {
      std::vector<int> vec = geneVector(1, 10);
      std::vector<int> v = filter([](auto x){return x > 5;}, vec);
      assert( v == geneVector(6, 10));
    }
    
    {
        std::vector<int> vec = geneVector(1, 10);
        std::vector<int> v = filter([](auto x){return x > 10;}, vec);
        assert( v == geneVector(6, 5)); // empty vector
    }
    
    {
        std::vector<int> vec = geneVector(1, 10);
        std::vector<int> v = take<int>(1, vec);
        assert( v == geneVector(1, 1) );

    }
    {
        vector<int> v1;
        std::vector<int> vec = geneVector(0, 0);
        std::vector<int> v = drop<int>(1, vec);
        assert( v == v1);
        assert( v.size() == 0 );
        
    }
    {
        vector<int> v1;
        std::vector<int> vec = geneVector(1, 10);
        std::vector<int> v = drop<int>(10, vec);
        assert( v == v1);
        assert( v.size() == 0 );
        
    }
    {
        vector<int> v1 = geneVector(1, 10);
        std::vector<int> vec = geneVector(1, 10);
        std::vector<int> v = drop<int>(0, vec);
        assert( v1 == vec);
    }
   
  
    {
        // test quickSortT
        int array[] = {2, 1};
        int expected[] = {1, 2};
        int lo = 0, hi = 1;
        const int len = hi - lo + 1;
        quickSortT(array, lo, hi);
        assert(compareArray(array, expected, len) == true);
    }
    {
        // test quickSortT
        int array[] = {1, 2};
        int expected[] = {1, 2};
        int lo = 0, hi = 1;
        const int len = hi - lo + 1;
        quickSortT(array, lo, hi);
        assert(compareArray(array, expected, len) == true);
    }

    {
        // test quickSortT
        int array[] = {2};
        int expected[] = {2};
        int lo = 0, hi = 0;
        const int len = hi - lo + 1;
        quickSortT(array, lo, hi);
        assert(compareArray(array, expected, len) == true);
    }

    {
        // test quickSortT
        int array[] = {9, 1, 2, 3, 4, 5, 6, 7, 0, 8};
        int expected[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int lo = 0, hi = 9;
        const int len = hi - lo + 1;
        quickSortT(array, lo, hi);
        assert(compareArray(array, expected, len) == true);
    }
    {
        // test quickSortT
        std::vector<int> vec = {9, 1, 2, 3, 4, 5, 6, 7, 0, 8};
        int expected[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int* arr = &vec[0];
        int lo = 0, hi = 9;
        const int len = hi - lo + 1;
        quickSortT(arr, lo, hi);
        assert(compareArray(arr, expected, len) == true);
    }
    {
        {
            std::pair<string, string> p = split("cat", 1);
            string prefix = p.first;
            string suffix = p.second;
            assert(prefix == "c");
            assert(suffix == "at");
        }
        {
            std::pair<string, string> p = split("cat", 0);
            string prefix = p.first;
            string suffix = p.second;
            assert(prefix == "");
            assert(suffix == "cat");
        }
        
        {
            std::pair<string, string> p = split("cat", 2);
            string prefix = p.first;
            string suffix = p.second;
            assert(prefix == "ca");
            assert(suffix == "t");
        }
    }
    {
        string str = removeIndex("cat", 0);
        assert(str == "at");
        string str1 = removeIndex("cat", 1);
        assert(str1 == "ct");
        string str2 = removeIndex("cat", 2);
        assert(str2 == "ca");
    }
    {
        vector<int> v1 = geneVector(1, 4);
        vector<int> vec = take(0, v1);
        assert(vec.size() == 0);
        vector<int> vec1 = take(1, v1);
        assert(vec1.size() == 1);
        vector<int> vec2 = take(4, v1);
        assert(vec2.size() == 4);
        
    }
    
    {
        vector<int> v1 = geneVector(1, 4);
        vector<int> vec = drop(0, v1);
        assert(vec.size() == 4);
        vector<int> vec1 = drop(1, v1);
        assert(vec1.size() == 3);
        vector<int> vec2 = drop(4, v1);
        assert(vec2.size() == 0);
        
    }
    
    {
        vector<int> v1 = {1, 2, 3}; 
        vector<int> actualVal = AronLambda::init(v1);
        vector<int> expectedVal = {1, 2};
        assert(actualVal.size() == 2);
        assert(actualVal == expectedVal);
    }
    {
        vector<int> v1 = geneVector(1, 4);
        vector<int> vec = tail(v1);
        vector<int> v3 = {2, 3, 4};
        assert(vec.size() == 3);
        assert(vec == v3);
    }
    {
        vector<int> v1 = {1, 4, 9};
        vector<int> v2 = {2, 10};
        vector<int> v = mergeSortList(v1, v2);
        assert(v.size() == 5);
        vector<int> v3 = {1, 2, 4, 9, 10};
        assert(v == v3);
    }
    {
        int arr[] = {1, 2};
        int i=0;
        int j=1;
        swap<int>(arr, i, j);
        assert(arr[0] == 2);
        assert(arr[1] == 1);
    }
    {
        vec v(2);
        float arr[2] = {1.0, 2.0};
        v.createVec(arr, 2);
        v.print();
    }

    {
        // concate vec, concate two vec
        vec v1(2);
        vec v2(2);
        vec v4(2);
        vec v6(2);
        float arr1[2] = {1.0, 2.0};
        float arr4[2] = {2.0, 4.0};
        float arr6[2] = {3.0, 6.0};
        v1.createVec(arr1, 2);
        v2.createVec(arr1, 2);
        v4.createVec(arr4, 2);
        v6.createVec(arr6, 2);

        vec v3 = v1 + v2;
        vec v5 = v1 + (v2 + v1);
        assert(v3 == v4);
        assert(v5 == v6);
    }

    {
        // transpose vec to row
        vec v1(2);
        row r1(1, 2);
        float arr[2] = {1.0, 2.0};
        v1.createVec(arr, 2);
        r1.createRow(arr, 2);
        row r = v1.tran();
        assert(r == r1);
    }
    {
        // transpose row to vec
        vec v1(2);
        row r1(1, 2);
        float arr[2] = {1.0, 2.0};
        v1.createVec(arr, 2);
        r1.createRow(arr, 2);
        vec v = r1.tran();
        assert(v == v1);
    }
    {
        // generate matrix from 1 to n
        int ncol = 2;
        int nrow = 2;
        mat m(ncol, nrow);
        m.geneMat(1);
        assert(m.arr[0][0] == 1);
        assert(m.arr[0][1] == 2);
        assert(m.arr[1][0] == 3);
        assert(m.arr[1][1] == 4);
    }

    {
        // add two matrices
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;

        mat m2(ncol, nrow);
        m2.arr[0][0] = 1;
        m2.arr[0][1] = 1;
        m2.arr[1][0] = 1;
        m2.arr[1][1] = 1;

        mat m3 = m1 + m2;
        
        assert(m3.arr[0][0] == 3);
        assert(m3.arr[0][1] == 4);
        assert(m3.arr[1][0] == 5);
        assert(m3.arr[1][1] == 6);
    
        mat m4 = m1 + (m2 + m2);

        assert(m4.arr[0][0] == 4);
        assert(m4.arr[0][1] == 5);
        assert(m4.arr[1][0] == 6);
        assert(m4.arr[1][1] == 7);
    }
    {
        // 2 3
        // 4 5
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        vec v1 = m1.getVec(0);
        vec v2(2);
        v2.arr[0][0] = 2;
        v2.arr[1][0] = 4;
        assert(v1 == v2);

        vec v3(2);
        v3.arr[0][0] = 3;
        v3.arr[1][0] = 5;
        vec v4 = m1.getVec(1);
        assert(v3 == v4);
    }

    {
        // 2 3
        // 4 5
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        row r1 = m1.getRow(0);
        row r2(1, 2);
        r2.arr[0][0] = 2;
        r2.arr[0][1] = 3;
        assert(r1 == r2);

        row r3(1, 2);
        r3.arr[0][0] = 4;
        r3.arr[0][1] = 5;
        row r4 = m1.getRow(1);
        assert(r3 == r4);
    }

    
    {
        // dot product in column vector and row vector
        vec v1(2);
        v1.arr[0][0] = 1;
        v1.arr[1][0] = 2;

        row r1(1, 2);
        r1.arr[0][0] = 2;
        r1.arr[0][1] = 3;
        float d = r1.dot(v1);

        assert(d == 8);
    }
    {
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;

        vec v1(2);
        v1.arr[0][0] = 2;
        v1.arr[1][0] = 4;

        vec expv1 = m1.getVec(0);
        assert(expv1 == v1);

        vec v2(2);
        v2.arr[0][0] = 3;
        v2.arr[1][0] = 5;
        vec expv2 = m1.getVec(1);
        assert(expv2 == v2);
    }
    {
        // outer product = vec * row
        // [1]
        // [2]  [3, 4]
        vec v1(2);
        v1.arr[0][0] = 1;
        v1.arr[1][0] = 2;

        row r1(1, 2);
        r1.arr[0][0] = 3;
        r1.arr[0][1] = 4;
        
        mat m1(2, 2);
        //mat m1(2, 2);
        mat m2 = m1; // call copy constructor
        mat m3 = v1.multi();
        
        // DID not call copy constructor
        // not sure how it works here?
        mat m = v1 * r1;
        m.print();
        assert(m.arr[0][0] == 3);
        assert(m.arr[0][1] == 4);
        assert(m.arr[1][0] == 6);
        assert(m.arr[1][1] == 8);
    }
    {
        // outer product = vec * row
        // [1]
        // [2]  [3, 4]
        vec v1(1);
        v1.arr[0][0] = 1;

        row r1(1, 1);
        r1.arr[0][0] = 3;
        mat m = v1 * r1;
        assert(m.arr[0][0] == 3);
    }
    {
        // 2 3 [1]
        // 4 5 [2]
        vec v1(2);
        v1.arr[0][0] = 1;
        v1.arr[1][0] = 2;

        vec v3(2);
        v3.arr[0][0] = 8;
        v3.arr[1][0] = 14;

        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;


        vec v2 = m1 * v1;
        assert(v2 == v3);
    }
    {
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.zero();
        assert(m1.arr[0][0] == 0);
        assert(m1.arr[0][1] == 0);
        assert(m1.arr[1][0] == 0);
        assert(m1.arr[1][1] == 0);
    }
    {
        // 2 3
        // 4 5
        //
        // [[2]
        // [4]] [[2],[3]]
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        
        mat m2(ncol, nrow);
        m2.arr[0][0] = 4;
        m2.arr[0][1] = 6;
        m2.arr[1][0] = 8;
        m2.arr[1][1] = 12;

        mat m = m1.getVec(0) * m1.getRow(0);
        assert(m2 == m);
    }
    {
        // 2 3
        // 4 5
        //
        // [[2]
        // [4]] [[2],[3]]
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        
        mat mz(ncol, nrow);
        mz.zero();
        
        mat m = m1 + mz;
        assert(m == m1);
    }
    {
        // 2 3
        // 4 5
        //
        // [[2]
        // [4]] [[2],[3]]
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        
        mat m2(ncol, nrow);
        m2.arr[0][0] = 4;
        m2.arr[0][1] = 6;
        m2.arr[1][0] = 8;
        m2.arr[1][1] = 12;
        
        mat mz(ncol, nrow);
        mz.zero();
        
        mat m = m1.getVec(0) * m1.getRow(0) + mz;
        assert(m2 == m);
    }
    {
        // matrix multiplication 
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        // 1 2
        // 3 4
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;
        
        mat m2(ncol, nrow);
        // 2 3
        // 4 5
        m2.arr[0][0] = 2;
        m2.arr[0][1] = 3;
        m2.arr[1][0] = 4;
        m2.arr[1][1] = 5;

        mat m3(ncol, nrow);
        m3.arr[0][0] = 10;
        m3.arr[0][1] = 13;
        m3.arr[1][0] = 22;
        m3.arr[1][1] = 29;

        mat m4 = m1*m2;
        assert(m3 == m4);
    }
    
    {
        // matrix assignment operator
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        // 1 2
        // 3 4
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;
        mat m2;
        m2 = m1;
        assert(m2 == m1);
    }
    {
        // matrix assignment operator
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        // 1 2
        // 3 4
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;
        mat m2;
        m2 = m1;
        assert(m2 == m1);
    }
    {
        // row vector assignment operator
        row rw(1, 2);
        rw.arr[0][0] = 1;
        rw.arr[0][1] = 2;
        
        row rw1(1, 2);
        rw1.arr[0][0] = 1;
        rw1.arr[0][1] = 2;

        row rw2 = rw;
        assert(rw2 == rw1);
        
        row rw3;
        rw3 = rw;
        assert(rw3 == rw1);
        
        // Chaining 
        rw3 = (rw1 = rw);
        assert(rw3 == rw);
        assert(rw1 == rw);
    }
    {
        vector<int> vec = {2, 1, 3};
        vector<int> v1 = quickSort(vec);
        vector<int> v2 = {1, 2, 3};
        assert(v1 == v2);
    }
    {
        vector<int> vec = {};
        vector<int> v1 = quickSort(vec);
        vector<int> v2 = {};
        assert(v1 == v2);
    }
    {
        vector<int> vec = {1};
        vector<int> v1 = quickSort(vec);
        vector<int> v2 = {1};
        assert(v1 == v2);
    }
    {
        vector<int> vec = {3, 1, 3, 4, 2, 2, 6, 0};
        vector<int> v1 = quickSort(vec);
        vector<int> v2 = {0, 1, 2, 2, 3, 3, 4, 6};
        assert(v1 == v2);
    }
    {
        int arr[] = {2, 1, 3, 5};
        int lo = 0;
        int hi = 3;
        mergeSort(arr, lo, hi);
        for(int i=0; i<4; i++){
            cout<<arr[i]<<std::endl;
        }
        
        int arr1[] = {1, 2, 3, 5};
        assert(arr[0] == arr1[0]);
        assert(arr[1] == arr1[1]);
        assert(arr[2] == arr1[2]);
        assert(arr[3] == arr1[3]);
    }
    {
        int arr[] = {2};
        int lo = 0;
        int hi = 0;
        mergeSort(arr, lo, hi);
        for(int i=0; i<1; i++){
            cout<<arr[i]<<std::endl;
        }
        
        int arr1[] = {2};
        assert(arr[0] == arr1[0]);
    }
    {
        vector<int> v1 = {1, 2, 3};
        vector<int> v2 = {10};
        vector<int> v3 = {11};
        vector<int> v4 = zipWith([](auto x, auto y) { return x+y;}, v1, v2);
        assert(v3 == v4);
    }
    {
        vector<int> v1 = {1, 2, 3};
        int c = 2; 
        vector<int> v2 = v1 * c; 
        vector<int> v3 = {2, 4, 6};
        assert(v2 == v3);
    }
    {
        vector<double> v1 = {1, 2, 3};
        double c = 2; 
        vector<double> v2 = v1 * c; 
        vector<double> v3 = {2, 4, 6};
        assert(v2 == v3);
    }
    {
        vector<double> v1 = {1, 2, 3};
        double c = 2; 
        vector<double> v2 = c * v1; 
        vector<double> v3 = {2, 4, 6};
        assert(v2 == v3);
    }
    {
        row r1(1, 2);
        row r2(1, 2);
        row r3(1, 2);
        r1.arr[0][0] = 2;
        r1.arr[0][1] = 3;

        r2.arr[0][0] = 1;
        r2.arr[0][1] = 2;

        row rw = r1 + r2;

        r3.arr[0][0] = 3;
        r3.arr[0][1] = 5;

        assert(rw == r3);
    }
    {
        row r1(1, 2);
        row r2(1, 2);
        row r3(1, 2);
        r1.arr[0][0] = 2;
        r1.arr[0][1] = 3;

        r2.arr[0][0] = 1;
        r2.arr[0][1] = 2;

        row rw = r1 - r2;

        r3.arr[0][0] = 1;
        r3.arr[0][1] = 1;

        assert(rw == r3);
    }
    {
        row r1(1, 2);
        row r2(1, 2);
        r1.arr[0][0] = 4;
        r1.arr[0][1] = 8;

        float f = 2;
        row rw = r1/f;

        r2.arr[0][0] = 2;
        r2.arr[0][1] = 4;

        assert(rw == r2);
    }
    {
        int ncol = 2;
        int nrow = 2;
        float f = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 4;
        m1.arr[1][0] = 8;
        m1.arr[1][1] = 10;

        mat m2(ncol, nrow);
        m2.arr[0][0] = 1;
        m2.arr[0][1] = 2;
        m2.arr[1][0] = 4;
        m2.arr[1][1] = 5;

        mat m3 = m1/f;

        assert(m2 == m3);
    }
    {
        int ncol = 2;
        int nrow = 2;
        float f = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 2;
        m1.arr[0][1] = 4;
        m1.arr[1][0] = 8;
        m1.arr[1][1] = 10;

        mat m2(ncol, nrow);
        m2.arr[0][0] = 4;
        m2.arr[0][1] = 8;
        m2.arr[1][0] = 16;
        m2.arr[1][1] = 20;

        mat m3 = m1*f;

        assert(m2 == m3);
    }
    {
        // 1 2 
        // 3 4 
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        mat m2 = m1.removeRow(0);

        mat m3(ncol - 1, nrow);
        m3.arr[0][0] = 3;
        m3.arr[0][1] = 4;

        assert(m2 == m3);
    }
    {
        // 1 2 
        // 3 4 
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        mat m2 = m1.removeVec(0);

        mat m3(ncol, nrow - 1);
        m3.arr[0][0] = 2;
        m3.arr[1][0] = 4;

        assert(m2 == m3);
        m1.print();
        m2.print();
    }
    {
        // 1 2 
        // 3 4 
        int ncol = 2;
        int nrow = 1;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[1][0] = 2;

        mat m2 = m1.removeVec(0);

        mat m3(ncol, nrow - 1);
        fl();
        m2.print();
    }
    {
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;
        mat m2 = m1.clone();

        assert(m1 == m2);
    }
    {
        // 1 2 2 3
        // 3 4 4 5 
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        mat m2(ncol, nrow);
        m2.arr[0][0] = 2;
        m2.arr[0][1] = 3;
        m2.arr[1][0] = 4;
        m2.arr[1][1] = 5;

        mat m4(ncol, 4);
        m4.arr[0][0] = 1;
        m4.arr[0][1] = 2;
        m4.arr[0][2] = 2;
        m4.arr[0][3] = 3;
        m4.arr[1][0] = 3;
        m4.arr[1][1] = 4;
        m4.arr[1][2] = 4;
        m4.arr[1][3] = 5;

        mat m3 = m1.concat(m2);

        m1.print();
        fl();
        m4.print();
        assert(m3 == m4);
    }
    {
        // 1 2 2 3
        // 3 4 4 5 
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        mat m2(ncol, nrow);
        m2.arr[0][0] = 2;
        m2.arr[0][1] = 3;
        m2.arr[1][0] = 4;
        m2.arr[1][1] = 5;

        mat m = concat(m1, m2);

        mat m4(ncol, 4);
        m4.arr[0][0] = 1;
        m4.arr[0][1] = 2;
        m4.arr[0][2] = 2;
        m4.arr[0][3] = 3;
        m4.arr[1][0] = 3;
        m4.arr[1][1] = 4;
        m4.arr[1][2] = 4;
        m4.arr[1][3] = 5;

        m.print();
        fl();
        m4.print();
        assert(m == m4);
    }
    {
        // 1 0 
        // 0 1 
        int ncol = 2;
        int nrow = 2;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 0;
        m1.arr[1][0] = 0;
        m1.arr[1][1] = 1;

        mat m2(ncol, nrow);
        m2.identity();
        assert(m1 == m2);
    }
    {
        fl();
        // 1 2 1 0 
        // 3 4 0 1 
        //

        int ncol  = 2;
        int nrow1 = 2;
        int nrow2 = 2;
        mat m1(ncol, nrow1);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;


        mat m2(ncol, nrow2);
        m2.identity();

        mat m3(ncol, nrow1 + nrow2);
        m3.arr[0][0] = 1;
        m3.arr[0][1] = 2;
        m3.arr[0][2] = 1;
        m3.arr[0][3] = 0;

        m3.arr[1][0] = 3;
        m3.arr[1][1] = 4;
        m3.arr[1][2] = 0;
        m3.arr[1][3] = 1;

        mat m = concat(m1, m2); 

        assert(m == m3);
        m.print();
        fl();
        m3.print();
    }
    {
        {
            int ncol  = 2;
            int nrow  = 2;
            mat m1(ncol, nrow);
            m1.arr[0][0] = 1;
            m1.arr[0][1] = 0;
            m1.arr[1][0] = 0;
            m1.arr[1][1] = 1;
            
            mat mid(ncol,nrow);
            mat m2 = mid.id();
            assert(m1 == m2);
        }
        {
            int ncol  = 1;
            int nrow  = 1;
            mat m1(ncol, nrow);
            m1.arr[0][0] = 1;
            
            mat mid(ncol,nrow);
            mat m2 = mid.id();
            assert(m1 == m2);
        }
    }
    
    {
        {
            vector<int> v = {2, 1, 3};
            vector<int> v1 = mergeSort(v);
            vector<int> v2 = {1, 2, 3}; 
            assert(v1 == v2);  
        }
        {
            vector<int> v = {1};
            vector<int> v1 = mergeSort(v);
            vector<int> v2 = {1}; 
            assert(v1 == v2);  
        }
        {
            vector<int> v = {2, 1};
            vector<int> v1 = mergeSort(v);
            vector<int> v2 = {1, 2};
            assert(v1 == v2);  
        }
        {
            vector<int> v = {1, 2, 3};
            vector<int> v1 = mergeSort(v);
            vector<int> v2 = {1, 2, 3};
            assert(v1 == v2);  
        }
    }
    {
        vector<int> v = {1, 2, 3};
        vector<int> v1= {3, 2, 1};
        vector<int> v2= reverse(v);
        assert(v1 == v2);
        assert(v1 == reverse(reverse(v1)));

    }
    {
        {
            vector<int> v = {1, 4, 5, 2, 3};
            vector<int> v1 = {1, 4};
            vector<int> v2 = takeWhile([](auto x){return x < 5;}, v);
            assert(v1 == v2);
        }
        {
            vector<int> v = {1, 4, 5, 2, 3};
            vector<int> v1 = {};
            vector<int> v2 = takeWhile([](auto x){return x < 0;}, v);
            assert(v1 == v2);
        }
    }
    {
        {
            vector<int> v = {1, 4, 5, 2, 3};
            vector<int> v1 = {5, 2, 3};
            vector<int> v2 = AronLambda::dropWhile([](auto x){return x < 5;}, v);
            assert(v1 == v2);
        }
        {
            vector<int> v = {1, 4, 5, 2, 3};
            vector<int> v1 = {1, 4, 5, 2, 3};
            vector<int> v2 = AronLambda::dropWhile([](auto x){return x < 0;}, v);
            assert(v1 == v2);
        }
    }
    {
        vector<vector<int>> vec1 = {
                        {0, 7, 7},
                        {0, 0, 7}
        };

        vector<vector<int>> vec2 = {
                        {0, 3, 3},
                        {0, 0, 3}
        };

        vector<vector<int>> vec3 = {
                        {0, 7, 7},
                        {0, 3, 3},
                        {0, 0, 7},
                        {0, 0, 3}
        };
        vector<vector<int>> vv = take(1, vec1);
        print(vec1);
        fl();
        print(vv);
        auto lam = [](auto v1, auto v2){
            auto n1 = takeWhile([](auto x){ return x == 0;}, v1).size();
            auto n2 = takeWhile([](auto x){ return x == 0;}, v2).size();
            return n1 <= n2;
        }; 

        vector<vector<int>> vecv = mergeSortListLam(lam, vec1, vec2);
        print(vecv);
        fl();
        print(vec3);
        assert(vecv == vec3);
    }
    {
        fl();
        vector<int> v = {1, 2, 3};
        int* arr = &v[0];
        for(int i=0; i<3; i++){
            print(arr[i]);
        }

        vector<vector<int>> vv = {
            {1, 2, 3},
            {3, 4, 5}
        };
        fl();
    }
    {
        fl();
        vector<vector<int>> vv = {
        {1, 2, 3},
        {4, 5, 6}
        };

        int ncol = vv.size();
        int nrow = vv[0].size();
        int** arr = vecVecToArrArr<int>(vv);
        for(int i=0; i<ncol; i++){
            for(int j=0; j<nrow; j++){
                pp(arr[i][j]);
            }
            pp("\n");
        }

        assert(arr[0][0] == vv[0][0]);
        assert(arr[0][1] == vv[0][1]);
        assert(arr[0][2] == vv[0][2]);
        assert(arr[1][0] == vv[1][0]);
        assert(arr[1][1] == vv[1][1]);
        assert(arr[1][2] == vv[1][2]);

        for(int i=0; i<ncol; i++)
            delete[] arr[i];
        
        delete[] arr;
    }
    {
        fl("arrArrToVecVec");
        {
            int ncol = 2;
            int nrow = 3;
            int** arr = allocateTemp<int>(ncol, nrow);
            arr[0][0] = 1;
            arr[0][1] = 2;
            arr[0][2] = 3;
            arr[1][0] = 4;
            arr[1][1] = 5;
            arr[1][2] = 6;

            vector<vector<int>> v3 = {
                {1, 2, 3},
                {4, 5, 6}
            };
            vector<vector<int>> vv = arrArrToVecVec<int>(arr, ncol, nrow);
            assert(arr[0][0] == v3[0][0]);
            assert(arr[0][1] == v3[0][1]);
            assert(arr[0][2] == v3[0][2]);
            assert(arr[1][0] == v3[1][0]);
            assert(arr[1][1] == v3[1][1]);
            assert(arr[1][2] == v3[1][2]);

            for(int i=0; i<ncol; i++)
                delete[] arr[i];
            delete[] arr;
        }
        {
            int ncol = 2;
            int nrow = 3;
            int arr[2][3] = {
                {1, 2, 3},
                {4, 5, 6}
            };
      
            
            vector<vector<int>> v3 = {
                {1, 2, 3},
                {4, 5, 6}
            };
            int* pt;
            pt = &arr[0][0];
            vector<vector<int>> vv = arrArrToVecVec<int>(&pt, ncol, nrow);
            assert(arr[0][0] == v3[0][0]);
            assert(arr[0][1] == v3[0][1]);
            assert(arr[0][2] == v3[0][2]);
            assert(arr[1][0] == v3[1][0]);
            assert(arr[1][1] == v3[1][1]);
            assert(arr[1][2] == v3[1][2]);
        }
    }
    {
        int ncol = 2;
        int nrow = 3;
        vector<vector<int>> vv = geneMatrix<int>(ncol, nrow, 1);

        vector<vector<int>> v2 = {
            {1, 2, 3},
            {4, 5, 6}
        };
        assert(vv == v2);
    }
    {
        fl();
        int ncol  = 2;
        int nrow1 = 2;
        int nrow2 = 2;
        mat m1(ncol, nrow1);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        mat m = m1.subMatrix(0, 0);
        m.print();
        fl();
        m1.print();
        assert(m == m1);
    }
    {
        fl();
        int ncol1 = 2;
        int nrow1 = 2;
        mat m1(ncol1, nrow1);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        int ncol2 = 1;
        int nrow2 = 2;
        mat m2(ncol2, nrow2);
        m2.arr[0][0] = 3;
        m2.arr[0][1] = 4;

        mat m3(ncol2, nrow2);
        m3.arr[0][0] = 3;
        m3.arr[0][1] = 4;
        
        mat m = m1.subMatrix(1, 0);
        m.print();
        fl();
        m2.print();
        assert(m == m2);
    }
    {
        fl();
        int ncol1 = 2;
        int nrow1 = 2;
        mat m1(ncol1, nrow1);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        mat m = m1.block(0, ncol1, 0, nrow1);
        assert(m == m1);
    }
    {
        fl();
        int ncol1 = 2;
        int nrow1 = 2;
        mat m1(ncol1, nrow1);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        int ncol2 = 1;
        int nrow2 = 2;
        mat m3(ncol2, nrow2);
        m3.arr[0][0] = 3;
        m3.arr[0][1] = 4;

        int clen = 1;
        int rlen = 2;

        mat m = m1.block(1, clen, 0, rlen);
        assert(m == m3);
    }
    {
        fl();
        int ncol1 = 2;
        int nrow1 = 2;
        mat m1(ncol1, nrow1);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        int ncol2 = 1;
        int nrow2 = 2;
        mat m3(ncol2, nrow2);
        m3.arr[0][0] = 1;
        m3.arr[0][1] = 2;

        int clen = 1;
        int rlen = 2;

        mat m = m1.block(0, clen, 0, rlen);
        assert(m == m3);
    }
    {
        fl();
        int ncol1 = 2;
        int nrow1 = 2;
        mat m1(ncol1, nrow1);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[1][0] = 3;
        m1.arr[1][1] = 4;

        int ncol2 = 1;
        int nrow2 = 1;
        mat m3(ncol2, nrow2);
        m3.arr[0][0] = 4;

        int clen = 1;
        int rlen = 1;

        mat m = m1.block(1, clen, 1, rlen);
        assert(m == m3);
    }
    {
        /*
        1 2 3    1 4
        4 5 6 => 2 5
                 3 6
        */
        fl();
        int ncol = 2;
        int nrow = 3;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[0][2] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        m1.arr[1][2] = 6;

        mat m3(nrow, ncol);
        m3.arr[0][0] = 1;
        m3.arr[0][1] = 4;
        m3.arr[1][0] = 2;
        m3.arr[1][1] = 5;
        m3.arr[2][0] = 3;
        m3.arr[2][1] = 6;

        mat m2 = m1.transpose();
        assert(m2 == m3);

        mat m4 = m1.transpose().transpose();
        assert(m1 == m4);
    }
    {
        /*
        1 2 3 
        4 5 6 
        */
        fl();
        int ncol = 2;
        int nrow = 3;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[0][2] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        m1.arr[1][2] = 6;

        mat m2(2, 1);
        m2.arr[0][0] = 1;
        m2.arr[1][0] = 4;

        mat m3 = m1.take(1);
        assert(m2 == m3);

        mat m4(2, 2);
        m4.arr[0][0] = 2;
        m4.arr[0][1] = 3;
        m4.arr[1][0] = 5;
        m4.arr[1][1] = 6;

        mat m5 = m1.drop(1);
        assert(m4 == m5);
        assert(m1 == concat(m2, m4));
    }
    {

        /*
        1 2 3 
        4 5 6 
        */
        fl();
        int ncol = 2;
        int nrow = 3;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[0][2] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        m1.arr[1][2] = 6;

        mat m2(ncol, 0);

        mat m3 = m1.take(0);
        m3.print();
        assert(m2 == m3);
    }
} //end if(false)
    {
        /*
        1 2 3 
        4 5 6 
        */
        fl();
        int ncol = 2;
        int nrow = 3;
        mat m1(ncol, nrow);
        m1.arr[0][0] = 1;
        m1.arr[0][1] = 2;
        m1.arr[0][2] = 3;
        m1.arr[1][0] = 4;
        m1.arr[1][1] = 5;
        m1.arr[1][2] = 6;

        mat m2(ncol, 0);

        mat m3 = m1.drop(nrow);
        m3.print();
        assert(m2 == m3);
    }
    {
        /*
        1 2 3 
        4 5 6 
        */
        fl();
        const int ncol = 2;
        const int nrow = 3;
        mat m1(ncol, nrow);
        m1.geneMat(1);

        mat m2(ncol, nrow-1);
        m2.arr[0][0] = 1;
        m2.arr[0][1] = 2;
        m2.arr[1][0] = 4;
        m2.arr[1][1] = 5;

        mat m3 = m1.init();
        m3.print();
        assert(m2 == m3);
    }
    {
        /*
        1 2 3 
        4 5 6 
        */
        fl();
        const int ncol = 2;
        const int nrow = 3;
        mat m1(ncol, nrow);
        m1.geneMat(1);

        mat m2(ncol, nrow-1);
        m2.arr[0][0] = 2;
        m2.arr[0][1] = 3;
        m2.arr[1][0] = 5;
        m2.arr[1][1] = 6;

        mat m3 = m1.tail();
        m3.print();
        assert(m2 == m3);
    }
    {
        /*
        1  
        4  
        */
        fl();
        const int ncol = 2;
        const int nrow = 1;
        mat m1(ncol, nrow);
        m1.geneMat(1);

        mat m2(ncol, 0);

        mat m3 = m1.tail();
        m3.print();
        assert(m2 == m3);
    }
    {
        /*
         insertVecNext(0, v1)
         1 2 3   9
         4 5 6   9

         1 9 2 3
         4 9 5 6 
         */
        {
            fl();
            const int ncol = 2;
            const int nrow = 3;
            mat m1(ncol, nrow);
            m1.geneMat(1);

            mat m2(ncol, nrow + 1);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 9;
            m2.arr[0][2] = 2;
            m2.arr[0][3] = 3;
            m2.arr[1][0] = 4;
            m2.arr[1][1] = 9;
            m2.arr[1][2] = 5;
            m2.arr[1][3] = 6;

            vec v1(2);
            v1.arr[0][0] = 9;
            v1.arr[1][0] = 9;

            mat m3 = m1.insertVecNext(0, v1);
            m3.print();
            assert(m2 == m3);
        }

        /*
         insertVecNext(2, v1)
         1 2 3   9
         4 5 6   9

         1 2 3 9
         4 5 6 9
         */
        {
            fl();
            const int ncol = 2;
            const int nrow = 3;
            mat m1(ncol, nrow);
            m1.geneMat(1);

            mat m2(ncol, nrow + 1);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 2;
            m2.arr[0][2] = 3;
            m2.arr[0][3] = 9;
            m2.arr[1][0] = 4;
            m2.arr[1][1] = 5;
            m2.arr[1][2] = 6;
            m2.arr[1][3] = 9;

            vec v1(2);
            v1.arr[0][0] = 9;
            v1.arr[1][0] = 9;

            mat m3 = m1.insertVecNext(2, v1);
            m3.print();
            assert(m2 == m3);
        }
    }
    {
        {
            /*
             insertVecPrevious(0, v1)
             1 2 3   9
             4 5 6   9

             9 1 2 3 
             9 4 5 6
             */
            fl();
            const int ncol = 2;
            const int nrow = 3;
            mat m1(ncol, nrow);
            m1.geneMat(1);

            mat m2(ncol, nrow + 1);
            m2.arr[0][0] = 9;
            m2.arr[0][1] = 1;
            m2.arr[0][2] = 2;
            m2.arr[0][3] = 3;
            m2.arr[1][0] = 9;
            m2.arr[1][1] = 4;
            m2.arr[1][2] = 5;
            m2.arr[1][3] = 6;

            vec v1(2);
            v1.arr[0][0] = 9;
            v1.arr[1][0] = 9;

            mat m3 = m1.insertVecPrevious(0, v1);
            m3.print();
            assert(m2 == m3);
        }
        {
            /*
             insertVecPrevious(0, v1)
             1 2 3   9
             4 5 6   9

             1 2 9 3 
             4 5 9 6
             */
            fl();
            const int ncol = 2;
            const int nrow = 3;
            mat m1(ncol, nrow);
            m1.geneMat(1);

            mat m2(ncol, nrow + 1);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 2;
            m2.arr[0][2] = 9;
            m2.arr[0][3] = 3;
            m2.arr[1][0] = 4;
            m2.arr[1][1] = 5;
            m2.arr[1][2] = 9;
            m2.arr[1][3] = 6;

            vec v1(2);
            v1.arr[0][0] = 9;
            v1.arr[1][0] = 9;

            mat m3 = m1.insertVecPrevious(nrow-1, v1);
            m3.print();
            assert(m2 == m3);
        }
    }
    {
        {
            char s1[] = "dog";
            char s2[] = "cat";
            assert(compareCString(s1, s2) != 0);
        }
        {
            char s1[] = "dog";
            char s2[] = "dog";
            assert(compareCString(s1, s2) == 0);
        }
        {
            string s = "123";
            assert(stringToInt(s) == 123);
        }
        {
            const char* pt = "dog";
            string s = "dog";
            assert(cStringToString(pt).compare(s) == 0);
        }
        {
            string s = "d";
            assert(charToString('d').compare(s) == 0);
        }
    }
    {
        {
            Point<double> p0(1, 1);
            Point<double> p1(2, 2);
            Point<double> p2(3, 3);
            int b = isColinear3(p0, p1, p2);
            assert(b == 0);
        }
        {
            Point<double> p0(3, 4);
            Point<double> p1(1, 1);
            Point<double> p2(2, 2);
            double b = isColinear3(p0, p1, p2);
            assert(b != 0);
        }
        {
            Point<double> p0(1, 1);
            Point<double> p1(2, 2);
            Point<double> p2(0, 0);
            int b = isColinear3(p0, p1, p2);
            assert(b == 0);
        }
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(1.0, 1.0);
            assert(p0 == p1);
        }
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(1.0, 1.1);
            assert(p0 != p1);
        }
        {
            Point<double> p0(0.0, 0);
            Point<double> p1(0, 0.0);
            assert(p0 == p1);
        }
        {
            Point<double> p0(1.0001, 1.0001);
            Point<double> p1(1.0001, 1.0001);
            assert(p0 == p1);
        }
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(1.0, 1.000001);
            assert(p0 != p1);
        }
    }
    {
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(2.0, 2.0);
            Point<double> p2(3.0, 3.0);
            Segment<double> seg(p0, p1);
            assert(seg.isColinear(p2) == true);
        }
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(2.0, 2.0);
            Point<double> p2(3.0, 3.00001);
            Segment<double> seg(p0, p1);
            assert(seg.isColinear(p2) == false);
        }

    }
    {
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(2.0, 2.0);
            Point<double> p2(3.0, 3.0);
            Segment<double> seg1(p0, p1);
            Segment<double> seg2(p0, p2);
            assert(seg1.squareDist() == (1 + 1));
            assert(seg2.squareDist() == (4 + 4));
        }
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(2.0, 2.0);
            Point<double> p2(1.0, 1.0);
            Segment<double> seg1(p0, p1);
            assert(seg1.isEndPoint(p2) == true);
        }
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(2.0, 2.0);
            Point<double> p2(1.0, 1.00001);
            Segment<double> seg1(p0, p1);
            assert(seg1.isEndPoint(p2) == false);
        }
        {
            Point<double> p0(1.0, 1.0);
            Point<double> p1(2.0, 2.0);
            Segment<double> seg1(p0, p1);
            assert(seg1.distance() > 1.413 && seg1.distance() < 1.415);
        }
    }
    {
        {
            /**
                1 2 x1 = 3
                0 5 x2 = 5
                
                x1 = 1 
                x2 = 1 
            */
            const int ncol = 2;
            const int nrow = 2;
            mat m2(ncol, nrow);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 2;
            m2.arr[1][0] = 0;
            m2.arr[1][1] = 5;

            
            vec b(2);
            b.arr[0][0] = 3;
            b.arr[1][0] = 5;

            vec expx(2);
            expx.arr[0][0] = 1;
            expx.arr[1][0] = 1;
            
            vec x = backwardSubstitute(m2, b);
            assert(x == expx);
        }
        {
            const int ncol = 1;
            const int nrow = 1;
            mat m2(ncol, nrow);
            m2.arr[0][0] = 2;
            
            vec b(1);
            b.arr[0][0] = 4;
            
            vec expx(1);
            expx.arr[0][0] = 2;
            
            vec x = backwardSubstitute(m2, b);
            assert(x == expx);
        }
        {
            const int ncol = 3;
            const int nrow = 3;
            mat m2(ncol, nrow);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 2;
            m2.arr[0][2] = 3;
            m2.arr[1][0] = 0;
            m2.arr[1][1] = 4;
            m2.arr[1][2] = 5;
            m2.arr[2][0] = 0;
            m2.arr[2][1] = 0;
            m2.arr[2][2] = 7;
            
            vec b(3);
            b.arr[0][0] = 6;
            b.arr[1][0] = 9;
            b.arr[2][0] = 7;
            
            vec expx(3);
            expx.arr[0][0] = 1;
            expx.arr[1][0] = 1;
            expx.arr[2][0] = 1;
            
            vec x = backwardSubstitute(m2, b);
            assert(x == expx);
        }
		{
		  vector<float> v = {1, 2, 3};
		  vec v1(v);
		  
		  assert(v1.ncol == 3);
		  assert(v1.arr[0][0] == 1);
		  assert(v1.arr[1][0] == 2);
		  assert(v1.arr[2][0] == 3);
		}
		{
		  vector<float> v = {1, 2, 3};
		  vec v1(v);
		  vec expx(3);
		  expx.arr[0][0] = 1;
		  expx.arr[1][0] = 2;
		  expx.arr[2][0] = 3;
		  bool b = v1 == expx;
		  assert(v1 == expx);
		  assert(b == true);
		}
    }
    {
        {
            
            /**
             1 2 3
             4 5 6
             7 8 9
             */
            const int ncol = 3;
            const int nrow = 3;
            mat m2(ncol, nrow);
            mat m3(ncol, nrow);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 2;
            m2.arr[0][2] = 3;
            m2.arr[1][0] = 4;
            m2.arr[1][1] = 5;
            m2.arr[1][2] = 6;
            m2.arr[2][0] = 7;
            m2.arr[2][1] = 8;
            m2.arr[2][2] = 9;
            
            m3.arr[0][0] = 1;
            m3.arr[0][1] = 0;
            m3.arr[0][2] = 0;
            m3.arr[1][0] = -4;
            m3.arr[1][1] = 1;
            m3.arr[1][2] = 0;
            m3.arr[2][0] = -7;
            m3.arr[2][1] = 0;
            m3.arr[2][2] = 1;

            mat m = ltri(m2, 0);
            assert(m == m3);
        }
        
        {
            /**
             1 2
             3 4
             */
            const int ncol = 2;
            const int nrow = 2;
            mat m2(ncol, nrow);
            mat m3(ncol, nrow);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 2;
            m2.arr[1][0] = 3;
            m2.arr[1][1] = 4;
            
            m3.arr[0][0] = 1;
            m3.arr[0][1] = 0;
            m3.arr[1][0] = -3;
            m3.arr[1][1] = 1;
            
            mat m = ltri(m2, 0);
            assert(m == m3);
        }
        {
            /**
             1 2 3
             4 5 6
             7 8 9
             */
            const int ncol = 3;
            const int nrow = 3;
            mat m2(ncol, nrow);
            mat m3(ncol, nrow);
            m2.arr[0][0] = 1;
            m2.arr[0][1] = 1;
            m2.arr[0][2] = 1;
            m2.arr[1][0] = 2;
            m2.arr[1][1] = 3;
            m2.arr[1][2] = 5;
            m2.arr[2][0] = 4;
            m2.arr[2][1] = 6;
            m2.arr[2][2] = 77;
            
            m3.arr[0][0] = 1;
            m3.arr[0][1] = 0;
            m3.arr[0][2] = 0;
            m3.arr[1][0] = -4;
            m3.arr[1][1] = 1;
            m3.arr[1][2] = 0;
            m3.arr[2][0] = -7;
            m3.arr[2][1] = 0;
            m3.arr[2][2] = 1;
            
            mat m0 = ltri(m2, 0);
            fl();
            m2.print();
            fl();
            m0.print();
            fl();
            mat m1 = ltri(m0*m2, 1);
            m1.print();
            fl();
            mat m11 = m1*m0*m2;
            m11.print();
            
            std::pair<mat, mat> lu = utri(m2);
            fl();
            fl();
            mat l = lu.first;
            mat u = lu.second;
            fl();
            l.print();
            fl();
            u.print();
            fl();
            std::pair<mat, mat> llu = utri(l);
            mat ll = llu.first;
            mat uu = llu.second;
            fl();
            fl();
            ll.print();
            fl();
            uu.print();
            mat id = l*ll;
            fl();
            id.print();
        }
        
    }
    {
        {
            vector<int> vec = {2, 3, 5, 7};
            vector<int> v = allPrime(10);
            assert(v == vec);
        }
        {
            vector<int> vec = {2};
            vector<int> v = allPrime(2);
            assert(v == vec);
        }
        {
            vector<int> vec = {};
            vector<int> v = allPrime(1);
            assert(v == vec);
        }

    }
    {
        std::vector<glm::vec3> vect;
        for(int i=0; i<3; i++){
            glm::vec3 v3;
            v3.x = 1.0;
            v3.y = 2.0;
            v3.z = 3.0;
            vect.push_back(v3);
        }
        float expected[] = {
            1.0, 2.0, 3.0,
            1.0, 2.0, 3.0,
            1.0, 2.0, 3.0
        };
        
        float* arr = (float*)vect.data();
        assert(compareArray(arr, expected, 9) == true);
    }
    {
        class Pt{
        public:
            int x;
            int y;
        public:
            Pt(){}
            Pt(int x_, int y_){
                x = x_;
                y = y_;
            }
        };
        std::vector<Pt> vect;
        Pt p1;
        p1.x = 1;
        p1.y = 2;
        Pt p2;
        p2.x = 3;
        p2.y = 4;

        vect.push_back(p1);
        vect.push_back(p2);
        int expected[] = {
            1, 2, 3, 4
        };
        int* ptrToData = (int*) vect.data();
        assert(compareArray(ptrToData, expected, 4) == true);
    }
    {
        class Pt{
        public:
            int x;
            int y;
        public:
            Pt(){}
            Pt(int x_, int y_){
                x = x_;
                y = y_;
            }
        };
        std::vector<Pt*> vect;
        vect.push_back(new Pt(1, 2));
        vect.push_back(new Pt(3, 4));
        int expected[] = {
            1, 2, 3, 4
        };
        assert(expected[0] == vect[0] -> x);
        assert(expected[1] == vect[0] -> y);
        assert(expected[2] == vect[1] -> x);
        assert(expected[3] == vect[1] -> y);
    }
    {
        int num = 3;
        int* ptr = &num;
        printf("%d", *ptr);
        assert((*ptr) == 3);
    }
    {
        {
            string str = "dog";
            std::pair<string, string> p = split(str, -1);
            assert(p.first == "");
            assert(p.second == "dog");
        }
        {
            string str = "dog";
            std::pair<string, string> p = split(str, 0);
            assert(p.first == "");
            assert(p.second == "dog");
        }
        {
            string str = "dog";
            std::pair<string, string> p = split(str, 1);
            assert(p.first == "d");
            assert(p.second == "og");
        }
        {
            string str = "dog";
            std::pair<string, string> p = split(str, 3);
            assert(p.first == "dog");
            assert(p.second == "");
        }
        {
            string str = "dog";
            std::pair<string, string> p = split(str, 5);
            assert(p.first == "dog");
            assert(p.second == "");
        }
    }
    {
        Rectangle* p = new Rectangle(1, 2);
        cout<<"rec area="<<p->area();
        Triangle* pt = new Triangle(1, 2);
        cout<<"tri area="<<pt -> area();
    }
    {
        {
            const char dog[] = "";
            string cat = "cat";
            string exp = "cat";
            string s = dog + cat;
            assert(exp == s);
        }
        {
            const char dog[] = "dog";
            string cat = "cat";
            string exp = "dogcat";
            string s = dog + cat;
            assert(exp == s);
        }
        {
            const char dog[] = "";
            string cat = "";
            string exp = "";
            string s = dog + cat;
            assert(exp == s);
        }

    }
    {
        {
            auto f = [](auto x, auto y) { return x + "/" + y;};
            vector<string> vec = {"dog", "cat"};
            // ["dog", "cat"]  ""
            string expectedVal("dog/cat/");
            string actualVal = foldr(f, c2s(""), vec); 

            assert(actualVal == expectedVal);
        }
        {
            auto f = [](auto x, auto y) { return x + "/" + y;};
            vector<string> vec = {"dog", "cat"};
            // ["dog", "cat"]  ""
            string expectedVal("dog/cat/k");
            string actualVal = foldr(f, c2s("k"), vec); 

            assert(actualVal == expectedVal);
        }
        {
            auto f = [](auto x, auto y) { return x - y;};
            vector<int> vec = {1, 2, 3};
            // ["dog", "cat"]  ""
            int expectedVal = 1;
            int actualVal = foldr(f, 1, vec); 

            assert(actualVal == expectedVal);
        }

        {
            auto f = [](auto x, auto y) { return x + "/" + y;};
            vector<string> vec = {"dog", "cat"};
            // ["dog", "cat"]  ""
            string expectedVal("/dog/cat");
            string actualVal = foldl(f, c2s(""), vec); 

            assert(actualVal == expectedVal);
        }
        {
            auto f = [](auto x, auto y) { return x + "/" + y;};
            vector<string> vec = {"dog", "cat"};
            // ["dog", "cat"]  ""
            string expectedVal("k/dog/cat");
            string actualVal = foldl(f, c2s("k"), vec); 

            assert(actualVal == expectedVal);
        }
        {
            auto f = [](auto x, auto y) { return x - y;};
            vector<int> vec = {1, 2, 3};
            // ["dog", "cat"]  ""
            int expectedVal = -5;
            int actualVal = foldl(f, 1, vec); 

            assert(actualVal == expectedVal);
        }
        
    }
    {
            {
                vector<int> v1 = {1, 2, 3};
                vector<int> v2 = {10, 20, 30, 40};
                vector<int> expectedVal = {1, 10, 2, 20, 3, 30};
                vector<int> actualVal = interleave(v1, v2);
                assert(actualVal == expectedVal);
            }
            {
                vector<int> v1 = {};
                vector<int> v2 = {10, 20, 30, 40};
                vector<int> expectedVal = {};
                vector<int> actualVal = interleave(v1, v2);
                assert(actualVal == expectedVal);
            }
    } 
    {
            {
                vector<int> v1 = {1, 2, 3};
                vector<int> expectedVal = {2};
                vector<int> actualVal = AronLambda::filter([](auto x){ return x % 2 == 0 ? true : false;}, v1);
                assert(actualVal == expectedVal);
            }
            {
                vector<int> v1 = {};
                vector<int> expectedVal = {};
                vector<int> actualVal = AronLambda::filter([](auto x){ return x % 2 == 0 ? true : false;}, v1);
                assert(actualVal == expectedVal);
            }
    }
    {
        {
            vector<string> v1 = {"", "dog"}; 
            vector<string> v2 = {"/", "/"};
            vector<string> v3 = {"", "/", "dog", "/"};
            vector<string> exp = interleave(v1, v2);
            assert(v3 == exp);
        }
    }
    {
        {
            vector<string> vec = splitStr("/f.x", "/");
            vector<string> exp = {"", "f.x"};
            assert(vec == exp);
        }
        {
            vector<string> vec = splitStr("/f.x/", "/");
            vector<string> exp = {"", "f.x", ""};
            assert(vec == exp);
        }
        {
            vector<string> vec = splitStr("dog", ".");
            vector<string> exp = {"dog"};
            assert(vec == exp);
        }
    }
    {
        {
            vector<string> v1 = {""}; 
            vector<string> v2 = {"/"};
            vector<string> v3 = interleave(v1, v2); 
            vector<string> exp = {"", "/"}; 
            assert(v3 == exp);
        }
    }
    {
        {
            string path1 = "/dog/cat/f.x";
            string path2 = "dog/cat/f.x";
            string path3 = "dog/cat";
            string path4 = "/f.x";  // => ["", "f.x"] => [""] ["/"]
            string path5 = "./f.x";  // => [".", "f.x"] => ["."] ["/"]
            string path6 = "./";  // => [".", "f.x"] => ["."] ["/"]
            string path7 = "/";  // => [".", "f.x"] => ["."] ["/"]
            string s1 = takeDirectory(path1);
            string s2 = takeDirectory(path2);
            string s3 = takeDirectory(path3);
            string s4 = takeDirectory(path4);
            string s5 = takeDirectory(path5);
            string s6 = takeDirectory(path6);
            string s7 = takeDirectory(path7);
            string exp1 = "/dog/cat";
            string exp2 = "dog/cat";
            string exp3 = "dog";
            string exp4 = "/";
            string exp5 = ".";
            string exp6 = ".";
            string exp7 = "/";
            assert(s1 == exp1);
            assert(s2 == exp2);
            assert(s3 == exp3);
            assert(s4 == exp4);
            assert(s5 == exp5);
            assert(s6 == exp6);
            assert(s7 == exp7);
        }
    }
    {
        {
            string fname = takeFileName("/f.x");
            string exp("f.x");
            assert(fname == exp);
        }
        {
            string fname = takeFileName("/dog/cat/f.x");
            string exp("f.x");
            assert(fname == exp);
        }
        {
            string fname = takeFileName("/dog/cat");
            string exp("cat");
            assert(fname == exp);
        }
        {
            string fname = takeFileName("/dog");
            string exp("dog");
            assert(fname == exp);
        }
        {
            string fname = takeFileName("dog/f.x");
            string exp("f.x");
            assert(fname == exp);
        }
        {
            string fname = takeFileName("f.x");
            string exp("f.x");
            assert(fname == exp);
        }
    }
    {
        {
            string s = takeExtension("f.x");
            assert(s == ".x");
        }
        {
            string s = takeExtension("f.");
            assert(s == ".");
        }
        {
            string s = takeExtension("/dog/cat.cow/f.x");
            assert(s == ".x");
        }
        {
            string s = takeExtension("dog");
            assert(s == "");
        }
        {
            string s = takeExtension(".x");
            assert(s == ".x");
        }
    }
    {
        {
            double x = 4.0;
            double e = sqrt(4);
            assert( fabs(e*e - x) < 0.00001 );
        }
        {
            double x = 0.1;
            double e = sqrt(0.1);
            assert( fabs(e*e - x) < 0.00001 );
        }

    }
    {
        {
            double x = nthRoot(0.3, 2);
            assert(fabs(x*x - 0.3) < 0.000001);
        }
        {
            double x = nthRoot(0.3, 1);
            assert(fabs(x - 0.3) < 0.000001);
        }
        {
            double x = nthRoot(0.3, 3);
            assert(fabs(x*x*x - 0.3) < 0.000001);
        }
        {
            double x = nthRoot(0.3, 4);
            assert(fabs(x*x*x*x - 0.3) < 0.000001);
        }
    }
    {
        {
            bool b = containStr("subject", "sub");
            assert(b);
        }
        {
            bool b = containStr("subject.x", ".x");
            assert(b);
        }
        {
            bool b = containStr("subject file.x", "file\\.x");
            assert(b);
        }
    }
    {
        {
            // match word
            regex rx("[a-z]+");
            bool b = containStrRegex(",,,,,", rx);
            assert(b == false);
        }
        {
            // match digits
            regex rx("[0-9]+");
            bool b = containStrRegex("1234 dog", rx);
            assert(b == true);
        }
        {
            // match digits
            regex rx("[0-9]+");
            bool b = containStrRegex("dog cat", rx);
            assert(b == false);
        }
        {
            // match file name with extension
            regex rx("\\.cpp$");
            bool b = containStrRegex("myfile.cppcat", rx);
            assert(b == false);
        }
        {
            // match file extension
            regex rx("\\.cpp$");
            bool b = containStrRegex("myfile.cpp", rx);
            assert(b == true);
        }
        {
            // match length of digits
            regex rx("[0-9]{3}");
            bool b = containStrRegex("dog cat 12abc", rx);
            assert(b == false);
        }
        {
            // match length of digits
            regex rx("[0-9]{3}");
            bool b = containStrRegex("dog cat 123abc", rx);
            assert(b == true);
        }
        {
            // match length of digits
            regex rx("[0-9]{2,5}");
            bool b = containStrRegex("dog cat 1234abc", rx);
            assert(b == true);
        }

    }
    {
        {
            vector<vector<int>> mat;
            vector<int> v1 = {};
            vector<int> v2 = {};
            vector<int> v3 = {};
            mat.push_back(v1);
            mat.push_back(v2);
            mat.push_back(v3);
            vector<vector<int>> expectedMat;
            vector<int> vv1 = {};
            // expectedMat.push_back(vv1);
            vector<vector<int>> actualMat = transpose(mat);

            assert(actualMat == expectedMat);
        }
        {
            vector<vector<int>> mat;
            vector<int> v1 = {1};
            vector<int> v2 = {2, 3, 4};
            vector<int> v3 = {5, 6, 7};
            mat.push_back(v1);
            mat.push_back(v2);
            mat.push_back(v3);
            vector<vector<int>> expectedMat;
            vector<int> vv1 = {1, 2, 5};
            expectedMat.push_back(vv1);
            vector<vector<int>> actualMat = transpose(mat);

            assert(actualMat == expectedMat);
        }
        {
            vector<vector<int>> mat;
            vector<int> v1 = {1, 0, 0};
            vector<int> v2 = {2, 3, 4};
            vector<int> v3 = {5, 6, 7};
            mat.push_back(v1);
            mat.push_back(v2);
            mat.push_back(v3);
            vector<vector<int>> expectedMat;
            vector<int> vv1 = {1, 2, 5};
            vector<int> vv2 = {0, 3, 6};
            vector<int> vv3 = {0, 4, 7};
            expectedMat.push_back(vv1);
            expectedMat.push_back(vv2);
            expectedMat.push_back(vv3);
            vector<vector<int>> actualMat = transpose(mat);

            assert(actualMat == expectedMat);
        }
        {
            vector<vector<int>> mat;
            vector<int> v1 = {1, 2, 3};
            mat.push_back(v1);
            vector<vector<int>> expectedMat;
            vector<int> vv1 = {1};
            vector<int> vv2 = {2};
            vector<int> vv3 = {3};
            expectedMat.push_back(vv1);
            expectedMat.push_back(vv2);
            expectedMat.push_back(vv3);
            vector<vector<int>> actualMat = transpose(mat);

            assert(actualMat == expectedMat);
        }
    }
    {
        {
            vector<int> v1 = {1, 2, 3};
            std::pair<vector<int>, vector<int>> actualPair = splitAt(0, v1);
            vector<int> fv = {};
            vector<int> sv = {1, 2, 3};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
        {
            vector<int> v1 = {1, 2, 3};
            std::pair<vector<int>, vector<int>> actualPair = splitAt(1, v1);
            vector<int> fv = {1};
            vector<int> sv = {2, 3};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
        {
            vector<int> v1 = {1, 2, 3};
            std::pair<vector<int>, vector<int>> actualPair = splitAt(2, v1);
            vector<int> fv = {1, 2};
            vector<int> sv = {3};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
    } 
    {
        {
            vector<int> v1 = {1, 2, 3};
            auto f = [](auto x) {return x == 4;};
            std::pair<vector<int>, vector<int>> actualPair = breakVec(f, v1);
            vector<int> fv = {};
            vector<int> sv = {1, 2, 3};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
        {
            vector<int> v1 = {1, 2, 3};
            auto f = [](auto x) {return x == 1;};
            std::pair<vector<int>, vector<int>> actualPair = breakVec(f, v1);
            vector<int> fv = {};
            vector<int> sv = {1, 2, 3};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
        {
            vector<int> v1 = {1, 2, 3};
            auto f = [](auto x) {return x == 2;};
            std::pair<vector<int>, vector<int>> actualPair = breakVec(f, v1);
            vector<int> fv = {1};
            vector<int> sv = {2, 3};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
        {
            vector<int> v1 = {1, 2, 3};
            auto f = [](auto x) {return x == 3;};
            std::pair<vector<int>, vector<int>> actualPair = breakVec(f, v1);
            vector<int> fv = {1, 2};
            vector<int> sv = {3};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
        {
            vector<int> v1 = {};
            auto f = [](auto x) {return x == 3;};
            std::pair<vector<int>, vector<int>> actualPair = breakVec(f, v1);
            vector<int> fv = {};
            vector<int> sv = {};
            std::pair<vector<int>, vector<int>> expectedPair = std::make_pair(fv, sv);
            assert(actualPair == expectedPair);
        }
    }
    {
        {
            vector<int> v1 = {1, 2, 3, 2};
            vector<int> actualVec = replaceVec(2, 9, v1); 
            vector<int> expectedVec = {1, 9, 3, 9};
            assert(actualVec == expectedVec);
        }
        {
            // x -> cp x
            vector<string> v1 = {"x", "->", "cp", "x"};
            vector<string> actualVec = replaceVec(c2s("x"), c2s("/file.x"), v1); 
            vector<string> expectedVec = {"/file.x", "->", "cp", "/file.x"}; 
            assert(actualVec == expectedVec);
        }
    }
    {
        {
            string s(" dog cat ");
            string actualVal = trimLeft(s);
            string expectedVal("dog cat ");
            assert(actualVal == expectedVal);
        }
        {
            string s(" dog cat ");
            string actualVal = trimRight(s);
            string expectedVal(" dog cat");
            assert(actualVal == expectedVal);
        }
        {
            string s(" dog cat ");
            string actualVal = trim(s);
            string expectedVal("dog cat");
            assert(actualVal == expectedVal);
        }
    }
    {
        {
            // x -> cp x
            std::pair<vector<string>, vector<string>> actualPair = parseLambda(c2s("  x  ->  cp  x   /tmp"));
            vector<string> fstVec = {"x"}; 
            vector<string> sndVec = {"cp", "x", "/tmp"}; 
            std::pair<vector<string>, vector<string>> expectedPair = std::make_pair(fstVec, sndVec); 
            pl(expectedPair.first);
            pl(expectedPair.second);
            assert( actualPair == expectedPair );
        }
    }
    {
        vector<string> vec = {"dog", "cat", "cow"};
        string actualVal = join(vec);
        string expectedVal = "dog cat cow";
        assert(actualVal == expectedVal); 
    }
    {
        // matrix, convert vec to vector
        {
            vec v(1);
            v.arr[0][0] = 1;
            vector<float> expectedVal = {1.0};
            vector<float> actualVal = v.toVector(); 
            assert(expectedVal == actualVal); 
        }
        {
            vec v(2);
            v.arr[0][0] = 1;
            v.arr[1][0] = 2;
            vector<float> expectedVal = {1.0, 2.0};
            vector<float> actualVal = v.toVector(); 
            assert(expectedVal == actualVal); 
        }
    }
    {
        // matrix, convert row to vector
        {
            row r(1, 1);
            r.arr[0][0] = 1;
            vector<float> expectedVal = {1.0};
            vector<float> actualVal = r.toVector(); 
            assert(expectedVal == actualVal); 
        }
        {
            row r(1, 2);
            r.arr[0][0] = 1;
            r.arr[0][1] = 2;
            vector<float> expectedVal = {1.0, 2.0};
            vector<float> actualVal = r.toVector(); 
            assert(expectedVal == actualVal); 
        }
    }
    {
        {
            row r1(1);
            row r2(1, 1);
            assert(r1.ncol == r2.ncol);
            assert(r1.nrow == r2.nrow);
        }
        {
            vec v(2);
            v.arr[0][0] = 1;
            v.arr[1][0] = 2;
            row tr = v.tran();
            row r1(2);
            r1.arr[0][0] = 1;
            r1.arr[0][1] = 2;
            // it seems to me it does not call my '==' from row class
            // assert(r1 == tr);
            assert(r1 == tr);
        }
    }
    {
        {
            // 2 3
            // 4 5
            int ncol = 2;
            int nrow = 2;
            mat m1(ncol, nrow);
            m1.arr[0][0] = 2;
            m1.arr[0][1] = 3;
            m1.arr[1][0] = 4;
            m1.arr[1][1] = 5;
            vector<float> v1 = {2.0, 4.0};
            vector<float> v2 = m1.getVec(0).toVector();
            assert(v1 == v2);
        }
        {
            // 2 3
            // 4 5
            int ncol = 2;
            int nrow = 2;
            mat m1(ncol, nrow);
            m1.arr[0][0] = 2;
            m1.arr[0][1] = 3;
            m1.arr[1][0] = 4;
            m1.arr[1][1] = 5;
            vector<float> v1 = {3.0, 5.0};
            vector<float> v2 = m1.getVec(1).toVector();
            assert(v1 == v2);
        }
        {
            // 2 3
            // 4 5
            int ncol = 2;
            int nrow = 2;
            mat m1(ncol, nrow);
            m1.arr[0][0] = 2;
            m1.arr[0][1] = 3;
            m1.arr[1][0] = 4;
            m1.arr[1][1] = 5;
            vector<float> v1 = {2.0, 3.0};
            vector<float> v2 = m1.getRow(0).toVector();
            assert(v1 == v2);
        }
        {
            // 2 3
            // 4 5
            int ncol = 2;
            int nrow = 2;
            mat m1(ncol, nrow);
            m1.arr[0][0] = 2;
            m1.arr[0][1] = 3;
            m1.arr[1][0] = 4;
            m1.arr[1][1] = 5;
            vector<float> v1 = {4.0, 5.0};
            vector<float> v2 = m1.getRow(1).toVector();
            assert(v1 == v2);
        }
    }
    {
        {

            // 2 3
            // 4 5
            int ncol = 2;
            int nrow = 2;
            mat m1(ncol, nrow);
            m1.arr[0][0] = 2;
            m1.arr[0][1] = 3;
            m1.arr[1][0] = 4;
            m1.arr[1][1] = 5;
            writeFile("/tmp/123.x", m1);
        }
    }
    {
        {
            int n = 3;
            string s = toStr(n);
            string s1 = "3";
            pp(s);
            assert(s == s1);
        }
        {
            float n1 = 3.101;
            string s1 = toStr(n1);
            string s2 = "3.101";
            assert(s1 == s2); 
        }
        {
            double n1 = 9.1;
            string s1 = toStr(n1);
            string s2 = "9.1";
            assert(s1 == s2);
        }
    }
    {
        {
            // insert a vector into other vector
            vector<int> vec1 = {1, 2, 3};
            vector<int> insertVec = {11, 22, 33};
            vector<int> expectedVec = {1, 11, 22, 33, 2, 3};
            vector<int> actualVec = insertAt(vec1, 1, insertVec);
            assert(actualVec == expectedVec);
            
        }
    }
    {
        // concate two vectors
        {
            vector<int> v1 = {1, 2, 3};
            vector<int> v2 = {11, 22};
            vector<int> v3 = v1 + v2;
            vector<int> expectedVal = {1, 2, 3, 11, 22};
            assert(v3 == expectedVal);
        }
    }
    {
        {
            using namespace SpaceComplex;
            Complex c1(1, 2);
            Complex c2(2, 3);
            Complex c4(3, 5);
            Complex c3 = c1 + c2;
            assert(c3 == c4);
        }
        {
            using namespace SpaceComplex;
            Complex c1(1, 2);
            Complex c2(2, 3);
            Complex c4(-1, -1);
            Complex c3 = c1 - c2;
            assert(c3 == c4);
        }
        {
            using namespace SpaceComplex;
            Complex c1(1, 2);
            Complex c2(2, 3);
            Complex c4(2 - 6, 3 + 4);
            Complex c3 = c1 * c2;
            assert(c3 == c4);
        }
        {
            using namespace SpaceComplex;
            Complex c1(1, 2);
            Complex c2(1, -2);
            Complex c3 = c1.conjugate();
            assert(c3 == c2);
        }
        {
            using namespace SpaceComplex;
            Complex c1(1, 2);
            Complex c2(1, 2);
            Complex c3 = c1/c2;
            Complex c4(1, 0);
            assert(c3 == c4);
        }
        {
            using namespace SpaceComplex;
            Complex c1(1, 2);
            Complex c2(1, 1);
            Complex c3 = c1/c2;
            Complex c4(1.5, 0.5);
            assert(c3 == c4);
        }
    }
    {
        using namespace MatrixVector;
        {
            vector<vector<float>> vec = {
                {1}
            };
            mat m(vec);
            mat m1(1, 1);
            m1.arr[0][0] = 1;
            
            assert(m == m1);
        }
        {
            vector<vector<float>> vec = {
                {1, 2, 3},
                {4, 5, 6}
            };
            mat m(vec);
            
            mat m1(2, 3);
            m1.arr[0][0] = 1;
            m1.arr[0][1] = 2;
            m1.arr[0][2] = 3;
            m1.arr[1][0] = 4;
            m1.arr[1][1] = 5;
            m1.arr[1][2] = 6;
            
            assert(m == m1);
        }
    }
    {
        {
            /**
             0 1 2
             3 4 5
             6 7 8
             */
            vector<vector<float>> vec2 = {
                {4, 5, 6},
                {1, 2, 3}
            };

            vector<vector<float>> vec = {
                {1, 2, 3},
                {4, 5, 6}
            };
            mat m(vec);
            mat m1 = m.swapRow(0, 1);
            mat expm(vec2);
            assert(m1 == expm);
            
        }
    }
    {
        const int a = 2;
        const int b = 2;
        int arr[a][b];
        arr[0][0] = 1;
        arr[0][1] = 1;
        arr[1][0] = 1;
        arr[1][1] = 1;
    }
    {
        pl("m << ");
        mat m(2, 3);
        m << 1, 2, 3,
             4, 5, 6;
        
        vector<vector<float>> vec = {
            {1, 2, 3},
            {4, 5, 6}
        };
        mat exp(vec);
        assert(m.ncol == exp.ncol);
        assert(m.nrow == exp.nrow);
        assert(m == exp);
    }
    {
        {
            vector<float> v = {2, 3, 4, 5, 6, 7};
            mat m(2, 3, v);
            
            mat expm(2, 3);
            expm.arr[0][0] = 2;
            expm.arr[0][1] = 3;
            expm.arr[0][2] = 4;
            expm.arr[1][0] = 5;
            expm.arr[1][1] = 6;
            expm.arr[1][2] = 7;
            
            assert(m.ncol == 2);
            assert(m.nrow == 3);
            assert(m == expm);
        }
        {
            vector<float> v = {};
            mat m(0, 0, v);
            mat expm(0, 0);
            
            assert(m.ncol == 0);
            assert(m.nrow == 0);
            assert(m == expm);
        }
        {
            vector<float> v = {2};
            mat m(1, 1, v);
            mat expm(1, 1);
            expm.arr[0][0] = 2;
            
            assert(m.ncol == 1);
            assert(m.nrow == 1);
            assert(m == expm);
        }

    }
    {
        using namespace Utility;
        using namespace std;
        {
            string sub = "";
            string s = "";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "";
            string s = "a";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "";
            string s = "abc";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "b";
            string s = "";
            bool b = isSubstring(sub, s);
            assert(b == false);
        }
        {
            string sub = "a";
            string s = "ba";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "a";
            string s = "ab";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "aa";
            string s = "aba";
            bool b = isSubstring(sub, s);
            assert(b == false);
        }
        {
            string sub = "aa";
            string s = "aaa";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "aa";
            string s = "ddaaa";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "abc";
            string s = "kabkkkabc";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
        {
            string sub = "abc";
            string s = "kabkkkabck";
            bool b = isSubstring(sub, s);
            assert(b == true);
        }
    }
    {
        using namespace Utility;
        using namespace std;
        {
            string sub = "";
            string s = "";
            int index = substringFirstIndex(sub, s);
            assert(index == -1);
        }
        {
            string sub = "";
            string s = "a";
            int index = substringFirstIndex(sub, s);
            assert(index == -1);
        }
        {
            string sub = "a";
            string s = "a";
            int index = substringFirstIndex(sub, s);
            assert(index == 0);
        }
        {
            string sub = "ab";
            string s = "a";
            int index = substringFirstIndex(sub, s);
            assert(index == -1);
        }
        {
            string sub = "a";
            string s = "ab";
            int index = substringFirstIndex(sub, s);
            assert(index == 0);
        }
        {
            string sub = "a";
            string s = "ba";
            int index = substringFirstIndex(sub, s);
            assert(index == 1);
        }
        {
            string sub = "ab";
            string s = "bab";
            int index = substringFirstIndex(sub, s);
            assert(index == 1);
        }
        {
            string sub = "ab";
            string s = "babk";
            int index = substringFirstIndex(sub, s);
            assert(index == 1);
        }
        {
            string sub = "abc";
            string s = "babkabbkkabck";
            int index = substringFirstIndex(sub, s);
            assert(index == 9);
        }
    }
    {
        vector<int> v = {1, 2, 3};
        vector<int>* ret = append(v, 4);
        vector<int>* expV = new vector<int>();
        expV -> push_back(1);
        expV -> push_back(2);
        expV -> push_back(3);
        expV -> push_back(4);

        assert((*expV) == (*ret));
    }
    {
        std::vector<std::string> v;
        std::string str = "example";
        v.push_back(std::move(str)); // str is now valid but unspecified
        str.back(); // undefined behavior if size() == 0: back() has a precondition !empty()
        str.clear(); // OK, clear() has no preconditions
    }
    {
        std::string str ("hello world.");
        str.back() = '!';
        std::cout << str << '\n';
    }
    {
        vector<int> vec = {2};
        int lo = 0;
        int hi = length(vec) - 1;
        int n = partition(vec, lo, hi);
        vector<int> expV = {2};
        assert(vec == expV);
    }
    {
        vector<int> vec = {2, 1};
        int lo = 0;
        int hi = length(vec) - 1;
        int n = partition(vec, lo, hi);
        vector<int> expV = {1, 2};
        assert(vec == expV);
    }
    {
        vector<int> vec = {2, 1, 4, 3};
        int lo = 0;
        int hi = length(vec) - 1;
        int n = partition(vec, lo, hi);
        vector<int> expV = {2, 1, 3, 4};
        assert(vec == expV);
    }
    {
        vector<int> vec = {2};
        int lo = 0;
        int hi = length(vec) - 1;
        int pivot = partitionInline(vec, lo, hi);
        vector<int> expV = {2};
        assert(pivot == 0);
        assert(vec == expV);
    }
    {
        vector<int> vec = {2, 1};
        int lo = 0;
        int hi = length(vec) - 1;
        int pivot = partitionInline(vec, lo, hi);
        vector<int> expV = {1, 2};
        assert(pivot == 1);
        assert(vec == expV);
    }
    {
        vector<int> vec = {2, 1, 4, 3};
        int lo = 0;
        int hi = length(vec) - 1;
        int pivot = partitionInline(vec, lo, hi);
        vector<int> expV = {2, 1, 3, 4};
        assert(pivot == 3);
        assert(vec == expV);
    }
    {
        vector<int> vec = {2, 3, 3, 3};
        int lo = 0;
        int hi = length(vec) - 1;
        int pivot = partitionInline(vec, lo, hi);
        vector<int> expV = {2, 3, 3, 3};
        assert(pivot == 3);
        assert(vec == expV);
    }
    {
        vector<int> vec = {1, 1};
        int lo = 0;
        int hi = length(vec) - 1;
        int pivot = partitionInline(vec, lo, hi);
        vector<int> expV = {1, 1};
        assert(pivot == 1);
        assert(vec == expV);
    }
    {
        {
            int arr[] = {1};
            int key = 1;
            int lo = 0;
            int hi = 0;
            bool b = binSearch(key, arr, lo, hi);
            t(b, true, "binSearch");
        }
        {
            int arr[] = {1, 2};
            int key = 2;
            int lo = 0;
            int hi = 1;
            bool b = binSearch(key, arr, lo, hi);
            t(b, true, "binSearch");
        }
        {
            int arr[] = {1, 2};
            int key = 1;
            int lo = 0;
            int hi = 1;
            bool b = binSearch(key, arr, lo, hi);
            t(b, false, "binSearch");
        }
        {
            int arr[] = {1, 2};
            int key = 3;
            int lo = 0;
            int hi = 1;
            bool b = binSearch(key, arr, lo, hi);
            t(b, false, "binSearch");
        }
        {
            int arr[] = {1, 2, 2, 2};
            int key = 2;
            int lo = 0;
            int hi = 3;
            bool b = binSearch(key, arr, lo, hi);
            t(b, true, "binSearch");
        }
        {
            int arr[] = {1, 2, 2, 2, 3};
            int key = 3;
            int lo = 0;
            int hi = 4;
            bool b = binSearch(key, arr, lo, hi);
            t(b, true, "binSearch");
        }

    }
    {
        {
            vector<vector<int>> v2 = {{1, 2}, {3, 4}};
            vector<vector<int>> expV = {{2, 4}, {6, 8}};
            vector<vector<int>> vv = 2*v2;
            assert(vv == expV);
        }
        {
            vector<vector<int>> v2 = {{1, 2}, {3, 4}};
            vector<vector<int>> expV = {{2, 4}, {6, 8}};
            vector<vector<int>> vv = v2*2;
            assert(vv == expV);
        }
        {
        }
    }
    {
        {
            vector<int> v = {};
            auto vv = splitWhen([](auto x) { return x % 2 == 0;}, v);
            vector<vector<int>> expV;
            assert(vv == expV);
        }
        {
            vector<int> v = {1, 2, 3, 4};
            auto vv = splitWhen([](auto x) { return x % 2 == 0;}, v);
            vector<vector<int>> expV = {{1}, {3}};
            assert(vv == expV);
        }
        {
            vector<string> v = {"a", "b", "c", "", "e", "", "f"};
            auto vv = splitWhen([](auto x) { return len(x) == 0;}, v);
            vector<vector<string>> expV = {{"a", "b", "c"}, {"e"}, {"f"}};
            assert(vv == expV);
        }
    }
    {
        {
            vector<int> vec = {1};
            vector<int> ret = removeIndex(vec, 0);
            vector<int> expV = {};
            assert(ret == expV);
        }
        {
            vector<int> vec = {1, 2, 3};
            vector<int> ret = removeIndex(vec, 0);
            vector<int> expV = {2, 3};
            assert(ret == expV);
        }
        {
            vector<int> vec = {1, 2, 3};
            vector<int> ret = removeIndex(vec, 2);
            vector<int> expV = {1, 2};
            assert(ret == expV);
        }
    }
    {
        {
            vector<int> vec = {1, 2, 3};
            vector<int> ret = removeIndexRange(vec, 0, 2);
            vector<int> expV = {};
            assert(ret == expV);
        }
        {
            vector<int> vec = {1, 2, 3};
            vector<int> ret = removeIndexRange(vec, 0, 0);
            vector<int> expV = {2, 3};
            assert(ret == expV);
        }
        {
            vector<int> vec = {1, 2, 3};
            vector<int> ret = removeIndexRange(vec, 0, 1);
            vector<int> expV = {3};
            assert(ret == expV);
        }
    }
    {
        {
            vector<vector<int>> vec2 = {{}, {4}};
            vector<int> ret = flat(vec2);
            vector<int> expV = {4};
            assert(ret == expV);
        }
        {
            vector<vector<int>> vec2 = {{4}};
            vector<int> ret = flat(vec2);
            vector<int> expV = {4};
            assert(ret == expV);
        }
        {
            vector<vector<int>> vec2 = {{1, 2, 3}, {4}};
            vector<int> ret = flat(vec2);
            vector<int> expV = {1, 2, 3, 4};
            assert(ret == expV);
        }
    }
    {
      {
	vector<int> v1 = {1};
	add(v1, 2);
	vector<int> expV = {1, 2};
	assert(v1 == expV);
      }
      {
      	vector<int> v1 = {};
	vector<int> expV = {1};
	add(v1, 1);
	assert(v1 == expV);
      }
      {
      	vector<float> v1 = {1.0};
	vector<float> expV = {1.0, 0.3};
	add(v1, (float)0.3);
	assert(v1 == expV);
      }
    }
    {
      using namespace AronPrint;
      {
	vector<float> v1 = {};
	vector<float> v2 = {10, 20, 30, 40};
	vector<float> v3 = {100, 200, 300, 400, 500};
	vector<float> expVec = {};
	vector<float> vec = interleave(v1, v2, v3);
	assert(vec == expVec);
      }
      {
	
	vector<float> v1 = {1};
	vector<float> v2 = {10, 20, 30, 40};
	vector<float> v3 = {100, 200, 300, 400, 500};
	vector<float> expVec = {1, 10, 100};
	vector<float> vec = interleave(v1, v2, v3);
	pp(expVec);
	pp(len(vec));
	pp(vec);
	assert(vec == expVec);
	
      }
      {
	
	vector<float> v1 = {1, 2, 3};
	vector<float> v2 = {10, 20, 30, 40};
	vector<float> v3 = {100, 200, 300, 400, 500};
	vector<float> expVec = {1, 10, 100,  2, 20, 200, 3, 30, 300};
	vector<float> vec = interleave(v1, v2, v3);
	assert(vec == expVec);
	
      }
    }
    
    {
      // namespace Lambda;
      // string take(int n, char* ptr)
      {
        std::string s = "ijkl";
        const char* s4 = s.c_str();
        assert("ij" == take(2, s4));
      }
      {
        std::string s = "ijkl";
        const char* s4 = s.c_str();
        assert("" == take(0, s4));
      }
      {
        std::string s = "ijkl";
        const char* s4 = s.c_str();
        assert("i" == take(1, s4));
      }
      {
        std::string s = "ijkl";
        const char* s4 = s.c_str();
        assert("ijkl" == take(5, s4));
      }
    }
    {
      {
        vector<int>* vec = nPrime(1);
        vector<int> expVec = {2};
        assert(*vec == expVec);
      }
      {
        vector<int>* vec = nPrime(2);
        vector<int> expVec = {2, 3};
        assert(*vec == expVec);
      }
      {
        vector<int>* vec = nPrime(3);
        vector<int> expVec = {2, 3, 5};
        assert(*vec == expVec);
      }
      {
        vector<int>* vec = nPrime(4);
        vector<int> expVec = {2, 3, 5, 7};
        assert(*vec == expVec);
      }
      {
        vector<int>* vec = nPrime(10);
        vector<int> expVec = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
        assert(*vec == expVec);
      }
    }
    {
      {
          string str = "";
          auto s1 = takeWhile([](auto x){ return x == '-';}, str);
          assert(s1 == "");
      }
      {
          string str = "--ab-";
          auto s1 = takeWhile([](auto x){ return x == '-';}, str);
          assert(s1 == "--");
      }
      {
          string str = "--ab-";
          auto s1 = dropWhile([](auto x){ return x == '-';}, str);
          assert(s1 == "ab-");
      }
      {
          string str = "a";
          auto s1 = takeWhile([](auto x){ return x == '-';}, str);
          assert(s1 == "");
      }
      {
          string str = "a";
          auto s1 = dropWhile([](auto x){ return x == '-';}, str);
          assert(s1 == "a");
      }
      {
          string str = "--ab";
          auto s1 = takeWhile([](auto x){ return x == '-';}, str);
          assert(s1 == "--");
      }
      {
          string str = "--ab";
          auto s1 = dropWhile([](auto x){ return x == '-';}, str);
          assert(s1 == "ab");
      }
      {
          string str = "--ab";
          auto s1 = takeWhile([](auto x){ return x == '-';}, str);
          auto s2 = dropWhile([](auto x){ return x == '-';}, str);
          assert(str == (s1 + s2));
      }
    }
    {
      {
        string str = "--ab";
        auto f = [](auto x) { return x == '-';};
        two<string, string> tw = splitWhileStr(f, str);
        string expx = "--";
        string expy = "ab";
        assert(tw.x == expx);
        assert(tw.y == expy);
      }
      {
        string str = "--";
        auto f = [](auto x) { return x == '-';};
        two<string, string> tw = splitWhileStr(f, str);
        string expx = "--";
        string expy = "";
        assert(tw.x == expx);
        assert(tw.y == expy);
      }
      {
        string str = "ab";
        auto f = [](auto x) { return x == '+';};
        two<string, string> tw = splitWhileStr(f, str);
        string expx = "";
        string expy = "ab";
        assert(tw.x == expx);
        assert(tw.y == expy);
      }
      {
        string str = "--ab--";
        auto f = [](auto x) { return x == '-';};
        two<string, string> tw = splitWhileStr(f, str);
        string expx = "--";
        string expy = "ab--";
        assert(tw.x == expx);
        assert(tw.y == expy);
      }
      {
        string str = "ab--";
        auto f = [](auto x) { return x == '-';};
        two<string, string> tw = splitWhileStr(f, str);
        string expx = "";
        string expy = "ab--";
        assert(tw.x == expx);
        assert(tw.y == expy);
      }

    }
    {
      {
        regex rx("\\s+");
        string s = replace("   ", rx, "KK");
        assert(s == "KK");
      }
    }

    {
      {
        bool b = isEmptyLine(" ");
        assert(b == true);
      }
      {
        bool b = isEmptyLine("");
        assert(b == true);
      }
      {
        bool b = isEmptyLine(" a");
        assert(b == false);
      }
      {
        bool b = isEmptyLine("a ");
        assert(b == false);
      }
    }

    {
      {
        string s = removeAdjacentDuplicateChar("a", 'a');
        assert(s == "a");
      }
      {
        string s = removeAdjacentDuplicateChar("ab", 'a');
        assert(s == "ab");
      }
      {
        string s = removeAdjacentDuplicateChar("ab", 'b');
        assert(s == "ab");
      }
      {
        string s = removeAdjacentDuplicateChar("aba", 'b');
        assert(s == "aba");
      }
      {
        string s = removeAdjacentDuplicateChar("abaa", 'a');
        assert(s == "aba");
      }
      {
        string s = removeAdjacentDuplicateChar("abaa", 'b');
        assert(s == "abaa");
      }
      {
        string s = removeAdjacentDuplicateChar("//", '/');
        assert(s == "/");
      }
      {
        string s = removeAdjacentDuplicateChar("/", '/');
        assert(s == "/");
      }
      {
        string s = removeAdjacentDuplicateChar("/a/b", '/');
        assert(s == "/a/b");
      }
      {
        string s = removeAdjacentDuplicateChar("//a/b", '/');
        assert(s == "/a/b");
      }
      {
        string s = removeAdjacentDuplicateChar("/a//b", '/');
        assert(s == "/a/b");
      }
      {
        string s = removeAdjacentDuplicateChar("//a//b", '/');
        assert(s == "/a/b");
      }
      {
        string s = removeAdjacentDuplicateChar("abb", 'b');
        assert(s == "ab");
      }
      {
        string s = removeAdjacentDuplicateChar("a/b", '/');
        assert(s == "a/b");
      }
      {
        string s = removeAdjacentDuplicateChar("a//b", '/');
        assert(s == "a/b");
      }
      {
        vector<string> vec = {"a", "b", "c"};
        string s = concatValidPath(vec);
        assert(s == "/a/b/c");
      }
      {
        vector<string> vec = {"/a", "b", "c"};
        string s = concatValidPath(vec);
        assert(s == "/a/b/c");
      }
      {
        vector<string> vec = {"/a", "/b", "c"};
        string s = concatValidPath(vec);
        assert(s == "/a/b/c");
      }
      {
        vector<string> vec = {"/a", "/b", "/c"};
        string s = concatValidPath(vec);
        assert(s == "/a/b/c");
      }
      {
        vector<string> vec = {"/a", "/b", "/c/"};
        string s = concatValidPath(vec);
        assert(s == "/a/b/c/");
      }
      {
        vector<string> vec = {"/a", "/b", "//c"};
        string s = concatValidPath(vec);
        assert(s == "/a/b/c");
      }
    }
	{
	  vector<vector<float>> v2 = {{1, 2},
								  {3, 4}
	                             };
	  float n = det2<float>(v2);
	  float expn = -2;
	  assert(n == expn);
	}
}
TEST_CASE( "nothing", "[AronLib2]") {
    using namespace MatrixVector;
    using namespace Utility;
    using namespace AronLambda;
    using namespace Algorithm;
    using namespace AronGeometry;
    using namespace SpaceTest;
	{
	  assert(1 == 1);
	}
}
